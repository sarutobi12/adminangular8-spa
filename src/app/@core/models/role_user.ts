// tslint:disable-next-line: class-name
export interface Role_User {
    userName: string;
    roleId: number;
    roleName: string;
    status: boolean;
    createBy: string;
    createTime: Date;
}
