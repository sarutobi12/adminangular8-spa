export interface FunctionMenu {
  FunctionId: string;
  RoleId:string,
  FunctionName: string;
  ParentId: string;
  CanCreate: boolean;

  CanRead: boolean;

  CanUpdate: boolean;

  CanDelete: boolean;
}
