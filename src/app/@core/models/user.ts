import { Department } from './department';
import { Role_User } from './role_user';

export interface User {
  Id: string;
  FullName: string;
  BirthDay: string;
  Email: string;
  Address: string;
  UserName: string;
  PhoneNumber: string;
  Avatar: string;
  Status: string;
  Gender: string;
  DateCreated: Date;
  Roles: string[];
}

