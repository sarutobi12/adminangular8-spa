export interface Role {
  Id: string;
  Name: string;
  Description: string;
}
