export interface Department {
    departmentId: number;
    parentId: string;
    departmentName: string;
    description: string;
    phoneNumber: string;
    note: string;
    position: number;
    status: boolean;
    createBy: string;
    createTime: Date;
}
