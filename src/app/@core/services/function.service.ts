import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { FunctionMenu } from '../models/functionMenu';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class FunctionService {

  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getAll(roleId:string): Observable<FunctionMenu[]> {
    return this.http.get<FunctionMenu[]>(this.baseUrl + 'function/GetAllFunction/'+roleId);
  }

}
