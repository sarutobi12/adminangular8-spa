import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { User } from "../models/user";

@Injectable({
  providedIn: "root"
})
export class UserService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl + "user");
  }

  saveUser(model: any) {
    return this.http.post(this.baseUrl + "user", model);
  }

  updateStatus(userId: string) {
    return this.http.get(this.baseUrl + "user/UpdateStatus/" + userId);
  }

  deleteUser(userId: string) {
    return this.http.delete(this.baseUrl + "user/" + userId);
  }
}
