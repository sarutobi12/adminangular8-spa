import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseAuth = environment.apiUrl + 'Account/';
  jwtHelper = new JwtHelperService();
  decodeToken: any;
  constructor(private http: HttpClient, private router: Router) { }


  login(model: any) {
    return this.http.post(this.baseAuth + 'login', model).pipe(
      map((response: any) => {
        const user = response;
        if (user) {
          localStorage.setItem('token', user.token);
          this.decodeToken = this.jwtHelper.decodeToken(user.token);
          console.log(this.decodeToken);
        }
      })
    );
  }

  loggedIn() {
    const token = localStorage.getItem('token');
    return !this.jwtHelper.isTokenExpired(token);
  }

  getUserLogin() {
    const token = localStorage.getItem('token');
    if (token !== '') {
      return this.jwtHelper.decodeToken(token);
    } else {
      return null;
    }
  }

  logOut() {
    localStorage.removeItem('token');
    this.router.navigate(['/auth']);
  }

}
