import { Injectable } from '@angular/core';

declare let alertify: any;

@Injectable({
  providedIn: 'root'
})
export class AlertifyService {

  constructor() {

    alertify.set('notifier', 'position', 'top-right');
  }

  confirm(message: string, callback: () => any) {
    alertify.confirm(message, function (e) {
      if (e) {
        callback();
      } else { }
    }).setHeader('Do you want delete?');
  }

  success(message: string) {
    alertify.success(message, {});
  }

  error(message: string) {
    alertify.error(message);
  }

  warning(message: string) {
    alertify.warning(message);
  }

}
