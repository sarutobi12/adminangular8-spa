import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Role } from "../models/role";

@Injectable({
  providedIn: "root"
})
export class RoleService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getRoles(): Observable<Role[]> {
    return this.http.get<Role[]>(this.baseUrl + "role");
  }

  savePermission(model): Observable<any> {
    return this.http.post(this.baseUrl + "Role/SavePermission", model);
  }
}
