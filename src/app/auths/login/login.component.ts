import { Component, OnInit } from '@angular/core';
import { AlertifyService } from 'src/app/@core/services/alertify.service';
import { AuthService } from 'src/app/@core/services/auth.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: any = {};
  constructor(
    private alertify: AlertifyService,
    private authService: AuthService,
    private router: Router,
    private routeActive: ActivatedRoute
  ) {}

  ngOnInit() {}

  login() {
    this.authService.login(this.model).subscribe(
      res => {
        this.alertify.success('Login success test');
      },
      (error: HttpErrorResponse) => {
        if (error.status === 400) {
          this.alertify.error(error.error);
        } else {
          this.alertify.error(error.error);
        }
      },
      () => {
        const returnUrl = this.routeActive.snapshot.queryParams.returnUrl;
        if (returnUrl !== '' && returnUrl != null) {
          this.router.navigate([returnUrl]);
        } else { this.router.navigate(['/pages']); }
      }
    );
  }
}
