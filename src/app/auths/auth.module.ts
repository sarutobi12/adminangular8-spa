import { NgModule } from '@angular/core';
import { AuthComponent } from './auth.component';
import { LoginComponent } from './login/login.component';
import { AuthRoutingModule } from './auth-routing.module';
import { FormsModule } from '@angular/forms';
@NgModule({
    imports: [
        AuthRoutingModule,
        FormsModule
    ],
    declarations: [
        AuthComponent,
        LoginComponent
    ],
})

export class AuthModule {}
