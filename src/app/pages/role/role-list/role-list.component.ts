import { Component, OnInit } from "@angular/core";
import { AlertifyService } from "src/app/@core/services/alertify.service";
import { FunctionService } from "src/app/@core/services/function.service";
import { RoleService } from "src/app/@core/services/role.service";
import { Role } from "src/app/@core/models/role";

@Component({
  selector: "app-role-list",
  templateUrl: "./role-list.component.html",
  styleUrls: ["./role-list.component.css"]
})
export class RoleListComponent implements OnInit {
  roles: Role[];

  constructor(
    private alertifyService: AlertifyService,
    private roleService: RoleService,
    private functionService: FunctionService
  ) {}

  ngOnInit() {
    this.loadRoles();
  }

  loadRoles() {
    this.roleService.getRoles().subscribe(res => {
      this.roles = res;
    });
  }

  loadFunction(){

  }
}
