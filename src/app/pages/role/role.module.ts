import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RoleComponent } from "./role.component";
import { RoleListComponent } from "./role-list/role-list.component";
import { RoleRoutingModule } from "./role-routing.module";
import { RoleFunctionComponent } from "./role-function/role-function.component";
import { ModalModule } from "ngx-bootstrap";
import { EasyUIModule } from "ng-easyui/components/easyui/easyui.module";

@NgModule({
  imports: [
    CommonModule,
    RoleRoutingModule,
    ModalModule.forRoot(),
    EasyUIModule
  ],
  declarations: [RoleComponent, RoleListComponent, RoleFunctionComponent]
})
export class RoleModule {}
