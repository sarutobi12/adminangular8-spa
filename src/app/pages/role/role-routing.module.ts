import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { RoleListComponent } from './role-list/role-list.component';
import { AuthGuard } from 'src/app/@core/guards/auth.guard';

const routes: Routes = [{
    path: '',
    component: RoleListComponent,
    runGuardsAndResolvers: 'always',
    canActivate: [AuthGuard],
    children: [
        {
            path: 'list',
            component: RoleListComponent
        },
        { path: '', redirectTo: 'list', pathMatch: 'full' },
        { path: '**', redirectTo: 'list' },
    ]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})

export class RoleRoutingModule {
}
