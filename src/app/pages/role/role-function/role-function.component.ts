import { Component, OnInit, ViewChild } from "@angular/core";
import { ModalDirective } from "ngx-bootstrap";
import { Role } from "src/app/@core/models/role";
import { AlertifyService } from "src/app/@core/services/alertify.service";
import { FunctionService } from "src/app/@core/services/function.service";
import { unflattern } from "src/app/@core/helper/code-util";
import { RoleService } from 'src/app/@core/services/role.service';

@Component({
  selector: "app-role-function",
  templateUrl: "./role-function.component.html",
  styleUrls: ["./role-function.component.css"]
})
export class RoleFunctionComponent implements OnInit {
  @ViewChild("roleFunction", { static: false }) roleFunction: ModalDirective;
  data = [];
  dataUnflat = [];
  roleId: string;
  constructor(
    private alertifyService: AlertifyService,
    private funtionService: FunctionService,
    private roleSevice: RoleService
  ) { }

  ngOnInit() { }

  showFunctionModal(role: Role): void {
    this.roleId = role.Id;
    this.funtionService.getAll(this.roleId).subscribe(res => {
      this.data = res.map(x => {
        x.RoleId = role.Id;
        return x;
      });
      this.dataUnflat = unflattern(res);
      this.roleFunction.show();
    });
  }

  savePermission() {
    let model = {
      listPermmission: this.data,
      roleId: this.roleId
    };
    this.roleSevice.savePermission(model).subscribe(res => {
      this.alertifyService.success("Update permission success");
      this.roleFunction.hide();
    });
    console.log(this.data);
  }
  changeCheckbox(id: string, event: any, action: string) {
    var index = this.data.findIndex(x => x.FunctionId == id);
    let valueCheck = event.target.checked;
    if (action === "View") {
      this.data[index].CanRead = valueCheck;
    } else if (action === "Create") {
      this.data[index].CanCreate = valueCheck;
    } else if (action === "Update") {
      this.data[index].CanUpdate = valueCheck;
    } else {
      this.data[index].CanDelete = valueCheck;
    }
    console.log(this.data);
  }
}
