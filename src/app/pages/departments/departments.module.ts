import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DepartmentsRoutingModule } from './departments-routing.module';
import { DepartmentsComponent } from './departments.component';
import { DepartmentListComponent } from './department-list/department-list.component';

@NgModule({
    imports: [
        DepartmentsRoutingModule,
        CommonModule
    ],
    declarations: [
       DepartmentsComponent,
       DepartmentListComponent
    ],
})

export class DepartmentsModule {
}
