import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DepartmentsComponent } from './departments.component';
import { DepartmentListComponent } from './department-list/department-list.component';

const departmentsRoutes: Routes = [{
  path: '',
  component: DepartmentsComponent,
  children: [
    {
      path: 'departments',
      component: DepartmentListComponent,
    },
    { path: '', redirectTo: 'departments', pathMatch: 'full' },
    { path: '**', redirectTo: 'departments' },
  ]
}];

@NgModule({
  imports: [
    RouterModule.forChild(departmentsRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class DepartmentsRoutingModule { }