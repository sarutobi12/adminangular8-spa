import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-departments',
  template: `
  <router-outlet></router-outlet>
`,
})
export class DepartmentsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
