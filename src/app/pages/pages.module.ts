import { NgModule } from '@angular/core';

import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsersModule } from './users/users.module';
import { DepartmentsModule } from './departments/departments.module';
import { AuthGuard } from '../@core/guards/auth.guard';
import { RoleModule } from './role/role.module';

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    UsersModule,
    DepartmentsModule,
    RoleModule,
  ],
  declarations: [
    PagesComponent,
    DashboardComponent
  ],
  providers: [
    AuthGuard
  ]
})

export class PagesModule {
}
