import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './users.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { UserActionComponent } from './user-action/user-action.component';

const usersRoutes: Routes = [{
  path: '',
  component: UsersComponent,
  children: [
    {
      path: 'users',
      component: UserListComponent,
    },
    {
      path: 'user-edit',
      component: UserEditComponent,
    },
    {
      path: 'user-action',
      component: UserActionComponent,
    },
    { path: '', redirectTo: 'users', pathMatch: 'full' },
    { path: '**', redirectTo: 'users' },
  ]
}];

@NgModule({
  imports: [
    RouterModule.forChild(usersRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class UsersRoutingModule { }
