import {
  Component,
  OnInit,
  ViewChild,
  Output,
  EventEmitter
} from "@angular/core";
import { AlertifyService } from "src/app/@core/services/alertify.service";
import { BsModalRef, BsModalService, ModalDirective } from "ngx-bootstrap";
import {
  FormGroup,
  Validators,
  FormBuilder,
  FormGroupDirective
} from "@angular/forms";
import { MustMatch } from "src/app/@core/helper/code-util";
import { UserService } from "src/app/@core/services/user.service";
import { HttpErrorResponse } from "@angular/common/http";
import { Select2OptionData } from "ng-select2";
import { Options } from "select2";
import { RoleService } from "src/app/@core/services/role.service";
import { User } from 'src/app/@core/models/user';

@Component({
  selector: "app-user-action",
  templateUrl: "./user-action.component.html",
  styleUrls: ["./user-action.component.css"]
})
export class UserActionComponent implements OnInit {
  @Output() loadDataParent = new EventEmitter();
  @ViewChild("childModal", { static: false }) childModal: ModalDirective;
  registerForm: FormGroup;
  submitted = false;

  public listRoles: Array<Select2OptionData>;
  public options: Options = {
    width: "100%",
    multiple: true,
    tags: true
  };
  action: string;
  constructor(
    private alertify: AlertifyService,
    private userService: UserService,
    private roleService: RoleService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.getRoles();
    this.registerValidateForm();

  }

  get f() {
    return this.registerForm.controls;
  }

  registerValidateForm(){
    this.registerForm = this.formBuilder.group(
      {
        Id: [],
        username: ["", Validators.required],
        fullName: ["", Validators.required],
        address: ["", Validators.required],
        phoneNumber: ["", Validators.required],
        email: ["", [Validators.required, Validators.email]],
        password: ["", [Validators.required, Validators.minLength(6)]],
        confirmPassword: ["", Validators.required],
        status: ["", [Validators.required]],
        roles: []
      },
      {
        validator: MustMatch("password", "confirmPassword")
      }
    );
  }

  actionUser() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    console.log(this.registerForm.value);
    this.userService.saveUser(this.registerForm.value).subscribe(
      (res: any) => {
        if (res) {
          this.alertify.success("Add success");
          this.loadDataParent.emit();
          this.childModal.hide();
        }
      },
      (err: HttpErrorResponse) => {
        debugger;
        this.alertify.error(err.error);
      }
    );
  }

  getRoles() {
    this.roleService.getRoles().subscribe(
      res => {
        this.listRoles = res.map(item => {
          return { id: item.Name, text: item.Name };
        });
      },
      (err: HttpErrorResponse) => {
        this.alertify.error(err.error);
      }
    );
  }

  showChildModal(action: string, user: User = null): void {
    this.action = action;
    this.submitted = false;
    this.registerForm.reset();
    if (this.action === 'edit') {
      this.registerForm.setValue(
        {
          Id: user.Id,
          username: user.UserName,
          fullName: user.FullName,
          address: user.Address,
          phoneNumber: user.PhoneNumber,
          email: user.Email,
          password: "",
          confirmPassword: "",
          status: user.Status.toString(),
          roles: user.Roles
        }
      );
      this.registerForm.get('password').setErrors(null);
      this.registerForm.get('confirmPassword').setErrors(null);
    }
    this.childModal.show();
  }

}
