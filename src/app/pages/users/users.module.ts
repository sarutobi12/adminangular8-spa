import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap';

import { UsersRoutingModule } from './users-routing.module';
import { UserListComponent } from './user-list/user-list.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UsersComponent } from './users.component';
import { CommonModule } from '@angular/common';
import { UserEditComponent } from './user-edit/user-edit.component';
import { UserActionComponent } from './user-action/user-action.component';
import { NgSelect2Module } from 'ng-select2';


@NgModule({
    imports: [
        UsersRoutingModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ModalModule.forRoot(),
        NgSelect2Module
    ],
    declarations: [
        UsersComponent,
        UserListComponent,
        UserDetailComponent,
        UserEditComponent,
        UserActionComponent
    ],
    providers:[]
})

export class UsersModule {
}
