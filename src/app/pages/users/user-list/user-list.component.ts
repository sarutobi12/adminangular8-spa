import { Component, OnInit, TemplateRef } from "@angular/core";
import { UserService } from "src/app/@core/services/user.service";
import { ActivatedRoute } from "@angular/router";
import { User } from "src/app/@core/models/user";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { AlertifyService } from "src/app/@core/services/alertify.service";

@Component({
  selector: "app-user-list",
  templateUrl: "./user-list.component.html",
  styleUrls: ["./user-list.component.css"]
})
export class UserListComponent implements OnInit {
  users: User[];
  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private alertifyService: AlertifyService
  ) {}

  ngOnInit() {
    this.loadUsers();
  }
  loadUsers() {
    return this.userService.getUsers().subscribe(
      res => {
        this.users = res;
        console.log(res);
      },
      err => {
        console.log(err);
      }
    );
  }
  updateStatus(userId: string) {
    this.userService.updateStatus(userId).subscribe(res => {});
  }

  deleteUser(user: User) {
    this.alertifyService.confirm(user.UserName, () => {
      this.userService.deleteUser(user.Id).subscribe(res=>{
        this.loadUsers();
      },err=>{
        console.log("System Error")
      })
    });
  }
}
