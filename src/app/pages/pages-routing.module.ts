import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard } from '../@core/guards/auth.guard';

const routes: Routes = [{
    path: '',
    component: PagesComponent,
    runGuardsAndResolvers: 'always',
    canActivate: [AuthGuard],
    children: [
        {
            path: 'dashboard',
            component: DashboardComponent
        },
        {
            path: 'users',
            loadChildren: () => import('./users/users.module')
                .then(m => m.UsersModule),
        },
        {
          path: 'role',
          loadChildren: () => import('./role/role.module')
              .then(m => m.RoleModule),
      },
        {
            path: 'departments',
            loadChildren: () => import('./departments/departments.module')
                .then(m => m.DepartmentsModule),
        },
        { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
        { path: '**', redirectTo: 'dashboard' },
    ]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})

export class PagesRoutingModule {
}
