import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { ThemeModule } from './@theme/theme.module';
import { PagesModule } from './pages/pages.module';
import { JwtModule } from '@auth0/angular-jwt';
import { HttpClientModule } from '@angular/common/http';
import { RoleModule } from './pages/role/role.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthModule } from './auths/auth.module';
import { AlertifyService } from './@core/services/alertify.service';
import { UserService } from './@core/services/user.service';
import { RoleService } from './@core/services/role.service';

export function tokenGetter() {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ThemeModule,
    PagesModule,
    AuthModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter,
        whitelistedDomains:  ["localhost:44397"],
        blacklistedRoutes: ["localhost:44397/api/Account"]
      },
    }),

  ],
  providers: [
    AlertifyService,
    UserService,
    RoleService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
