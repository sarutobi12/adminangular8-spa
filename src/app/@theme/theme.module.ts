import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import {
    FooterComponent,
    HeaderComponent,
    SidebarComponent,
} from './components';
import { LayoutComponent } from './layout/layout.component';

const COMPONENTS = [
    HeaderComponent,
    SidebarComponent,
    FooterComponent,
    LayoutComponent
];

@NgModule({
    imports: [CommonModule, RouterModule],
    exports: [CommonModule, ...COMPONENTS],
    declarations: [...COMPONENTS],
})

export class ThemeModule { }