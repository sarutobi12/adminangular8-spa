import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/@core/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  user: any = {};
  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.getUserLogin();
  }

  getUserLogin() {
    this.user = this.authService.getUserLogin();
    console.log(this.user);
  }

  logOut() {
    this.authService.logOut();
  }


}
